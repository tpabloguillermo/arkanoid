// clase con una arreglo de tipo Posicion {x,y,color}
class AllPositions {
    private Positions bLocation[];

    public Positions[] getBLocation() {
        return bLocation;
    }

    public void setBLocation(Positions[] p) {
        this.bLocation = p;
    }

    @Override
    public String toString() {
        String s = new String();
        for(int i = 0; i < bLocation.length; i++){
            s += bLocation[i];
        }
        return s;
    }

}