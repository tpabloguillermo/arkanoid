import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;

public class Menu extends JPanel implements ActionListener {
	private String buttonLabel[] = { "Play", "Settings", "Ranking", "Exit" };
	private JButton button;
	private JLabel title;
	private Arkanoid juego;
	private Thread thread;
	private JFrame frame;
	private Image background_img;

	public Menu() {
		setLayout(new GridBagLayout());
		initComponents();
		setSize(800, 600);
		background_img = new ImageIcon("imagenes/background_img.jpg").getImage();
		frame = new JFrame();
		frame.add(this);
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		FXPlayer.ARKANOIDMENU.play();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(background_img, 0, 0, getWidth(), getHeight(), this);
	}

	private void initComponents() {
		title = new JLabel();
		title.setIcon(new ImageIcon("imagenes/gameicon.png"));
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 2;
		c.gridheight = 2;
		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 0;
		c.gridy = 0;
		add(title, c);
		for (int i = 0; i < buttonLabel.length; i++) {
			c.gridy += 2;
			button = new JButton(buttonLabel[i]);
			button.setIcon(new ImageIcon("imagenes/" + buttonLabel[i] + ".png"));
			button.setFocusable(false);
			button.addActionListener(this);
			button.setBackground(Color.WHITE);
			button.setOpaque(false);
			button.setBorderPainted(false);
			button.setForeground(Color.white);
			button.setFont(new Font("Helvetica", Font.BOLD, 18));
			this.add(button, c);
		}
	}

	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "Play": {
			FXPlayer.ARKANOIDMENU.stop();
			juego = new Arkanoid();

			thread = new Thread() {
				public void run() {
					juego.run(1.0 / 60.0);
				}
			};

			thread.start();
			break;
		}
		case "Settings": {
			JDialog jd = new JDialog();
			jd.setModal(true);
			jd.setLayout(new BorderLayout());
			SettingsPanel settings = new SettingsPanel("arkanoidProperties.properties");
			jd.add(settings);
			jd.setSize(800, 600);
			jd.setLocationRelativeTo(null);
			jd.setVisible(true);
			break;
		}
		case "Ranking": {
			JDialog jd = new JDialog();
			jd.setModal(true);
			jd.setLayout(new BorderLayout());
			Ranking ranking = new Ranking();
			jd.add(ranking);
			jd.setSize(800, 600);
			jd.setLocationRelativeTo(this);
			jd.setVisible(true);
			break;
		}
		case "Exit": {
			FXPlayer.ARKANOIDMENU.stop();
			frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			break;
		}
		}
	}

}
