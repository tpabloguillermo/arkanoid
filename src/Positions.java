
// clase para guardar los atributos de la posicion y el color del bloque que tomo del archiv JSON
public class Positions {
    protected int x,y;
    protected String color;

    public int getX() { return x; }

    public int getY() { return y; }

    public String getColor() { return color; }

    public void setX(int x) { this.x = x; }

    public void setY(int y) { this.y = y; }

    public void setColor(String color) { this.color = color; }


}