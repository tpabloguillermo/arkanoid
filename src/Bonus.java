class Bonus extends GraphicObject{
    protected String nombre;
    protected boolean caught = false;
    
    public Bonus(String nombre,Arkanoid ark){
    	super(ark);
		this.nombre = nombre;
		velocityY = 110.0;
		setImage("imagenes/"+nombre+".GIF");
    }
    
	double left() {
		return (getX());
	}

	double right() {
		return (getX() + getWidth());
	}

	double top() {
		return (getY() - getHeight() / 2.0);
	}

	double bottom() {
		return (getY() + getHeight() / 2.0);
	}
	
    public boolean getCaught() {
    	return this.caught;
    }
    public void setCaught() {
    	this.caught = true;
    }
    
    public String getName() {
    	return nombre;
    }

	void update(double delta) {
		setY(getY() + velocityY * delta);
	}
}