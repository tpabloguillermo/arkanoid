
public class Ball extends GraphicObject {
	private double velocityX, velocityY;
	private static double radius;
	private double ball_velocity = 200.0;
	private boolean run = false;
	private boolean duplicated = false;
	private static int seconds = 0;
	private String ball_img = "imagenes/ball.gif";

	public Ball(Arkanoid ark){
		super(ark);
		velocityX = ball_velocity;
		velocityY = ball_velocity;
		setImage(ball_img);
		radius = (getHeight() / 2);
	}

	double left() {
		return (getX() - radius);
	}

	double right() {
		return (getX() + radius);
	}

	double top() {
		return (getY() - radius);
	}

	double bottom() {
		return (getY() + radius);
	}

	public double getRadius() {
		return radius;
	}

	public double getVelocityX() {
		return velocityX;
	}

	public double getVelocityY() {
		return velocityY;
	}

	public void setVelocityX(double velX) {
		velocityX = velX;
	}

	public void setVelocityY(double velY) {
		velocityY = velY;
	}

	void update(double delta) {
		 // Disparos pelota
		if (ark.getKeyboard().isKeyPressed(Integer.parseInt(ark.gameProps.getProperty("SHOOTING"))) && !run) {
			run = true;
			FXPlayer.HITPAD.play();
		}
		
		// Movimiento de la pelota	
		if (run) {
			setX(getX() + getVelocityX() * delta);
			setY(getY() + getVelocityY() * delta);
			if (left() < ark.background.left()){ // Cuando llega a la parte izquierda de la ventana
				setVelocityX(-getVelocityX());
				FXPlayer.HITWALL.play();
			}
			else if (right() >  ark.background.right()){ // Cuando llega a la parte derecha de la ventana
				setVelocityX(-getVelocityX());
				FXPlayer.HITWALL.play();
			}
			if (top() < ark.background.top()) { // Cuando llega a la parte superior de la ventana
				setVelocityY(-getVelocityY());
				FXPlayer.HITWALL.play();

			} else if (bottom() > ark.background.bottom()) { // Cuando llega a la parte inferior de la ventana y cae
				seconds = 0; // Se vuelven a empezar a contar los segundos que definen el aumento o no ve la
								// velocidad de la nave
				setVelocityX(ball_velocity);
				setVelocityY(ball_velocity);// La velocidad de la bola vuelve a su estado inicial
				setPosition(ark.paddle.getX()+ ark.paddle.getSizeX() / 2, ark.paddle.getY() - getRadius() * 2);
				if (!duplicated)
					ark.die(); // Pierde una vida porque cayo
				else
					duplicated = false; // Si la pelota que cae es la duplicada, se desactiva el bonus
				run = false;
			}
			// Revisa el tiempo y cada 15 segundos aumenta la velocidad en 20

			if (ark.watch.getSeconds() == seconds + 20) { // Si pasaron 20 segundos despues del ultimo registro
				seconds = ark.watch.getSeconds(); // Toma el valor del tiempo que transcurre en el juego
				if (getVelocityX() > 0)
					setVelocityX(getVelocityX() + 30); // a medida que pasa el tiempo, aumenta la velocidad de la pelota
				else
					setVelocityX(getVelocityX() - 50);
				if (getVelocityY() > 0)
					setVelocityY(getVelocityY() + 50);
				else
					setVelocityY(getVelocityY() - 50);
			}
		}
		else {
			setPosition(ark.paddle.getX() + ark.paddle.getSizeX() / 2, ark.paddle.getY() - radius * 2);
		}
	}

	public void slow() {
		setVelocityX(ball_velocity);
		setVelocityY(ball_velocity);
	}

	public boolean isDuplicated() {
		return duplicated;
	}

	public void setDuplicated() {
		duplicated = true;
	}

	public void disableDuplicated() {
		duplicated = false;
	}

	public void stop() {
		setVelocityX(ball_velocity);
		setVelocityY(ball_velocity);// La velocidad de la bola vuelve a su estado inicial
		setPosition(ark.paddle.getX()+ ark.paddle.getSizeX() / 2, ark.paddle.getY() - getRadius() * 2);
		run = false;
	}
	
	void run() {
		run = true;
	}
	boolean isRunning() {
		return run;
	}
}
