import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;

class ScoreBoard extends GraphicObject {
	private String text = "";
	private String textResult = "";
	private String fontStr = "fuentes/arcade.ttf";
	private int titleHeight;
	private int lineNumber = 1;
	private static Font font;

	ScoreBoard(Arkanoid ark) {
		super(ark);
		registerFont(fontStr);
	}

	public void updateScoreboard() {
		if (ark.gameOver()) {
			textResult = "   GAME OVER\nYOUR SCORE WAS:\n    " + ark.getScore();
		} else if (ark.win()) {
			textResult = "   YOU WON!\nYOUR SCORE IS:\n     " + ark.getScore();

		} else {
			text = "Score: " + ark.getScore() + "\nLives: " + ark.getLives();
		}
	}

	public void draw(Graphics2D g) {
		g.setFont(font);
		g.setColor(Color.RED);
		g.drawString("HIGHSCORE", 575, 250);
		g.drawString("10000", 575, 280);
		g.setFont(font.deriveFont(16f));
		g.setColor(Color.white);
		g.drawString("Round " + ark.getLevel(), 580, 500);
		// Sigue actualizando puntaje
		lineNumber = 1;
		g.setColor(Color.WHITE);
		g.setFont(font.deriveFont(16f));
		FontMetrics fontMetrics = g.getFontMetrics(font.deriveFont(50f));
		titleHeight = fontMetrics.getHeight();
		for (String line2 : text.split("\n")) {
			g.drawString(line2, 580, ark.getHeight() / 2 + (titleHeight * lineNumber));
			lineNumber++;
		}
		if (ark.win() || ark.gameOver()) { // Escribe textos de 'ganaste' o 'perdiste'
			g.setColor(Color.WHITE);
			lineNumber = 1;
			g.setFont(font.deriveFont(30f));
			FontMetrics fm = g.getFontMetrics(font.deriveFont(40f));
			titleHeight = fm.getHeight();
			for (String line : textResult.split("\n")) {
				g.drawString(line, 80, 300 + (titleHeight * lineNumber));
				lineNumber++;
			}
		}
	}

	public static boolean registerFont(String fontFile) {
		boolean b = false;
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, new File(fontFile)).deriveFont(24f);
			GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
			b = true;
		} catch (FontFormatException | IOException e) {
			b = false;
		}
		return b;
	}

	double left() {
		return 0;
	}

	double right() {
		return 0;
	}

	double top() {
		return 0;
	}

	double bottom() {
		return 0;
	}

	void update(double delta) {
	}

}