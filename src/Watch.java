import java.util.Timer;
import java.util.TimerTask;

public class Watch {
	  private Timer timer = new Timer(); 
	    private int seconds=0;

	    //Clase interna que funciona como contador
	    class counter extends TimerTask {
	        public void run() {
	            seconds++;
	            }
	    }
	    //Crea un timer, inicia segundos a 0 y comienza a contar
	    public void counter()
	    {
	        this.seconds=0;
	        timer = new Timer();
	        timer.schedule(new counter(), 0, 1000);
	    }
	    //Detiene el contador
	    public void stop() {
	        timer.cancel();
	    }
	    //Metodo que retorna los segundos transcurridos
	    public int getSeconds()
	    {
	        return this.seconds;
	    }
}
