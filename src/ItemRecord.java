class ItemRecord {
    protected String name;
    protected int score;
    protected String date;
    protected int level;

    public ItemRecord() {
        name = "";
        score = 0;
        date = "";
        level = 0;
    }

    public Integer getScore() {
        return this.score;
    }

    public String getName() {
        return this.name;
    }

    public String getDate() {
        return this.date;
    }

    public Integer getLevel() {
        return this.level;
    }

    public void addRecord(String name, Integer score, String date, Integer level) {
        this.name = name;
        this.score = score;
        this.date = date;
        this.level = level;
    }
}