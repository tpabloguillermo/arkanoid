import java.awt.Graphics2D;


public class Background extends GraphicObject {
	private int width = ark.getWidth() - 266;
	private int height = ark.getHeight();
	public Background(Arkanoid ark) {
		super(ark);
		setImage("imagenes/" + 1 + ".png");
	}

	public void draw(Graphics2D g) {
		g.drawImage(getImage(), (int) super.getX(), (int) super.getY() + 20, ark.getWidth() - 266, ark.getHeight(),null);
	}

	double left() {
		return getX() + 20;
	}

	double right() {
		return width - 20;
	}

	double top() {
		return getY()+40;
	}

	double bottom() {
		return height;
	}

	void update(double delta) {
	}

}
