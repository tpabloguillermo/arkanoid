

import java.io.*;
import java.net.URL;
import javax.sound.sampled.*;


public enum FXPlayer {
   HITPAD("hitpad.wav"),
   HITWALL("hitwall.wav"),
   ARKANOIDMENU("ArkanoidMenu.wav"),
   GAMESYSTEM("gamesystem.wav"),
   GAMESTART("gamestart.wav"),
   HITBRICK("hitbrick.wav"),
   WARP("warp.wav");


   public static enum Volume {
      MUTE, LOW, MEDIUM, HIGH
   }

   public static Volume volume = Volume.LOW;


   private Clip clip;


   FXPlayer(String wav) {
      try {
         URL url = new File("sonidos/"+wav).toURI().toURL();
         AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);
         clip = AudioSystem.getClip();

         clip.open(audioInputStream);
      } catch (UnsupportedAudioFileException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      } catch (LineUnavailableException e) {
         e.printStackTrace();
      }
   }


   public void play() {
      if (volume != Volume.MUTE) {
         if (!clip.isRunning()){
         	  clip.setFramePosition(0);
         		clip.start();
         }
      }
   }
   public void stop() {
	   if (clip.isRunning()){
      		clip.stop();
   }
      }


   static void init() {
      values();
   }
}