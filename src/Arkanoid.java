import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.util.*;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import com.entropyinteractive.*;
import java.io.*;
import com.google.gson.*;

public class Arkanoid extends JGame {
	private Image gameicon;
	private Image warpicon;
	private static final int PLAYER_LIVES = 3;
	private static int score = 0;
	private static int lives;
	private static int level = 1;
	private boolean gameOver = false;
	private boolean win = false;
	private boolean catchBonus = false;
	private boolean warp = false;

	Watch watch;
	Ball ball;
	Ball duplicate;
	Paddle paddle;
	Background background;
	ScoreBoard scoreboard;

	LaserShooting shooting;
	private Vector<Brick> bricks = new Vector<Brick>();
	private Vector<Bonus> bonus = new Vector<Bonus>();
	private static final String fileProperties = "arkanoidProperties.properties";
	private static final String defaultfileProperties = "arkanoidDefaultProperties.properties";

	Properties defaultProps = new Properties();
	Properties gameProps;

	public Arkanoid() {
		super("Arkanoid", 800, 600, fileProperties);
		Properties defaultProps = new Properties();
		FXPlayer.GAMESTART.play();
		try {
			/*
			 * Se guardan las propiedades por default, siempre que se inicie el menu de
			 * juego estan cargadas por defecto
			 */
			defaultProps.setProperty("FullScreen", "false");
			defaultProps.setProperty("Sound", "true");
			defaultProps.setProperty("OriginalMusic", "true");
			defaultProps.setProperty("Paddle", "true");
			defaultProps.setProperty("RIGHT", "39");
			defaultProps.setProperty("LEFT", "37");
			defaultProps.setProperty("SHOOTING", "32");
			FileOutputStream output = new FileOutputStream(defaultfileProperties);
			defaultProps.store(output, "-- comentario --");
			output.close();

			gameProps = new Properties(defaultProps);

			FileInputStream in = new FileInputStream(fileProperties);

			// Se intentan cargar propiedade para la app desde un archivo que se supone
			// existe
			gameProps.load(in);
			in.close();
		} catch (Exception e) {
			System.out.println(e);
		}

		duplicate = new Ball(this);
		watch = new Watch();
		ball = new Ball(this);
		paddle = new Paddle(this);
		background = new Background(this);
		scoreboard = new ScoreBoard(this);

	}

	public void gameStartup() {
		try {
			lives = PLAYER_LIVES;
			getBrickDisposition();
			background.setPosition(0, 0);
			scoreboard.setPosition(550, 0);
			gameicon = new ImageIcon("imagenes/gameicon.png").getImage();
			warpicon = new ImageIcon("imagenes/WarpEscape.gif").getImage();
			paddle.setPosition((getWidth() - 270) / 2, getHeight() - 50.0);
			ball.setPosition(paddle.getX() + paddle.getSizeX() / 2, paddle.getY() - ball.getRadius() * 2);
			watch.counter();
		} catch (Exception e) {
		}
	}

	public void gameUpdate(double delta) {
		if (getKeyboard().isKeyPressed(KeyEvent.VK_ESCAPE)) { // Se cierra el juego al tocar Escape
			gameShutdown();
			FXPlayer.ARKANOIDMENU.play();
		}

		if (!win && !gameOver) {
			paddle.update(delta);
			ball.update(delta);
			if (duplicate.isDuplicated()) {
				duplicate.update(delta);
				checkCollisions(duplicate, paddle);
			}

			if (paddle.laser()) {
				shooting.update(delta);
			}

			// Colisiones nave bola
			if (ball.isRunning())
				checkCollisions(ball, paddle);

			scoreboard.updateScoreboard();

			// Colisiones bola y ladrillos
			for (int i = 0; i < bricks.size(); i++) {

				checkCollisions(ball, bricks.elementAt(i));
				if (duplicate.isDuplicated()) {
					checkCollisions(duplicate, bricks.elementAt(i));
				}
				if (paddle.laser()) {
					checkCollisions(shooting, bricks.elementAt(i));
				}

				if (bricks.elementAt(i).getDestroyed()) {
					if (bricks.elementAt(i).hasBonus()) {
						bricks.elementAt(i).getBonus().setPosition(bricks.elementAt(i).getX(),
								bricks.elementAt(i).getY());
						bonus.add(bricks.elementAt(i).getBonus());
					}
					bricks.removeElementAt(i);
					if (bricks.size() - bricksGold() == 0) {
						win = true;
						scoreboard.updateScoreboard();
						level++;
						changeLevel();
					}
				}

			}

			// Aparicion de bonus
			for (int i = 0; i < bonus.size(); i++) {
				bonus.elementAt(i).update(delta);
				checkCollisions(paddle, bonus.elementAt(i));
				if (bonus.elementAt(i).getCaught()) {
					if (paddle.isEnlarged() || duplicate.isDuplicated() || catchBonus || paddle.laser()) {
						paddle.disableEnlarged();
						duplicate.disableDuplicated();
						catchBonus = false;
						paddle.disableLaser();
					}
					switch (bonus.elementAt(i).getName()) {
					case "enlarge": {
						paddle.setEnlarged();
						break;
					}
					case "catch": {
						catchBonus = true;
						break;
					}
					case "duplicate": {
						duplicate.setDuplicated();
						duplicate.setPosition(paddle.getX() + paddle.getSizeX() / 2,
								paddle.getY() - ball.getRadius() * 2);
						duplicate.run();
						break;
					}
					case "extraplayer": {
						lives++;
						break;
					}
					case "slow": {
						ball.slow();
						break;
					}
					case "warp": {
						warp = true;
						break;
					}
					case "laser": {
						paddle.setLaser();
						shooting = new LaserShooting(this);
						shooting.setPosition(paddle.getX() + paddle.getSizeX() / 2.0 - shooting.getWidth() / 2,
								paddle.getY());
					}
						break;
					}
					bonus.removeElementAt(i);
					if (warp) {
						win = true;
						scoreboard.updateScoreboard();
						level++;
						if (level <= 3) {
							changeLevel();
						}
					}
				}
			}
		} else {
			if (gameOver) {
				String nombre = JOptionPane.showInputDialog(null, "Ingrese su nombre");
				RankingLoader cr = new RankingLoader(nombre, this);
				this.gameShutdown();
			}
			if (win) {
				if (getLevel() > 3) {
					String nombre = JOptionPane.showInputDialog(null, "Ingrese su nombre");                                                                                           level--;
					RankingLoader cr = new RankingLoader(nombre, this);
					FXPlayer.ARKANOIDMENU.play();
					this.gameShutdown();
				}
			}
		}
	}

	private int bricksGold() {
		int count = 0;
		for (int i = 0; i < bricks.size(); i++) {
			if (bricks.elementAt(i).getColor() == "Gold") {
				count++;
			}
		}
		return (count);
	}

	void checkCollisions(Ball ball, Paddle paddle) {
		if ((isIntersecting(ball, paddle))) {
			if (catchBonus) {
				ball.stop();
			}
			ball.setVelocityY(-ball.getVelocityY());
			if (ball.getX() < paddle.getX()) {
				ball.setVelocityX(-ball.getVelocityX());
			}
		}
	}

	void checkCollisions(Paddle paddle, Bonus bonus) {
		if ((isIntersecting(paddle, bonus))) {
			bonus.setCaught();
		}
	}

	void checkCollisions(LaserShooting laser, Brick brick) {
		if (isIntersecting(laser, brick) && !brick.getColor().equals("Gold")) {
			breakBrick(brick);
			laser.resetPosition();
		}
	}

	void checkCollisions(Ball ball, Brick brick) {
		if (isIntersecting(brick, ball)) {
			FXPlayer.HITBRICK.play();
			breakBrick(brick);

			double overlapLeft = ball.right() - brick.left();
			double overlapRight = brick.right() - ball.left();
			double overlapTop = ball.bottom() - brick.top();
			double overlapBottom = brick.bottom() - ball.top();

			boolean ballFromLeft = overlapLeft < overlapRight;
			boolean ballFromTop = overlapTop < overlapBottom;

			double minOverlapX = ballFromLeft ? overlapLeft : overlapRight;
			double minOverlapY = ballFromTop ? overlapTop : overlapBottom;

			if (minOverlapX < minOverlapY) {
				if (ballFromLeft)
					ball.setVelocityX(-ball.getVelocityX());
			} else {
				ball.setVelocityY(-ball.getVelocityY());
			}
		}
	}

	boolean isIntersecting(GraphicObject objectA, GraphicObject objectB) {
		return (objectA.right() >= objectB.left() && objectA.left() <= objectB.right()
				&& objectA.bottom() >= objectB.top() && objectA.top() <= objectB.bottom());
	}

	void breakBrick(Brick brick) {
		if (!brick.getColor().equals("Gold")) {
			brick.golpear();
			if (brick.cantGolpes() - brick.getGolpesDados() == 0) {
				brick.setDestroyed();
				increaseScore(brick);
			}
		}
	}

	public void gameDraw(Graphics2D g) {
		g.drawImage(gameicon, getWidth() - 250, getHeight() - 550, 250, 90, null);
		background.draw(g);
		for (int i = 0; i < bricks.size(); i++) {
			bricks.elementAt(i).draw(g);
		}
		for (int i = 0; i < bonus.size(); i++) {
			bonus.elementAt(i).draw(g);
		}
		ball.draw(g);
		if (duplicate.isDuplicated()) {
			duplicate.draw(g);
		}
		if (warp) {
			g.drawImage(warpicon, (int) background.right() - 5, (int) background.bottom() - 80, 25, 70, null);
			FXPlayer.WARP.play();
			paddle.warp();
			ball.setPosition(0, 0);
		}
		if (paddle.laser()) {
			shooting.draw(g);
		}

		if (paddle.right() < 535)
			paddle.draw(g);

		scoreboard.draw(g);

	}

	public void gameShutdown() {
		stop();
	}

	public void reset() {
		bonus.removeAllElements();
		bricks.removeAllElements();
		paddle.disableEnlarged();
		duplicate.disableDuplicated();
		catchBonus = false;
		paddle.setPosition((getWidth() - 270) / 2, getHeight() - 50.0);
		ball.setPosition(paddle.getX() + paddle.getSizeX() / 2, paddle.getY() - ball.getRadius() * 2);
		ball.stop();
		ball.slow();
		paddle.disableLaser();
		background.setImage("imagenes/" + level + ".png");
		lives = PLAYER_LIVES;
		warp = false;
		win = false;
		gameOver = false;
	}

	public void getBrickDisposition() {
		Gson gson = new Gson();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("levels/" + level + ".json"));
			AllPositions resultado = gson.fromJson(br, AllPositions.class);
			if (resultado != null) {
				for (Positions brick : resultado.getBLocation()) {
					Brick b = new Brick(brick.getX(), brick.getY(), brick.getColor(), this);
					this.bricks.add(b);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	void increaseScore(Brick brick) {
		score = score + brick.getScore();
		scoreboard.updateScoreboard();
	}

	void die() {
		lives--;
		scoreboard.updateScoreboard();
		if (lives == 0) {
			gameOver = true;
			FXPlayer.ARKANOIDMENU.play();
		}
	}

	public void changeLevel() {
		new Timer().schedule(new TimerTask() {
			public void run() {
				reset();
				gameStartup();
			}
		}, 5000); // espera 5 segundos para pasar al siguiente nivel
	}

	public int getScore() {
		return score;
	}

	public int getLives() {
		return lives;
	}

	public int getLevel() {
		return level;
	}

	public boolean gameOver() {
		return gameOver;
	}

	public boolean win() {
		return win;
	}

}
