import java.awt.Graphics2D;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;

class Brick extends GraphicObject {
    protected int puntaje; // puntaje que va a tener el ladrillo
    protected String color; // color del ladrillo
    protected Bonus bonus; // bonus que va contener
    protected int golpes;
    protected int golpesDados;
    protected Image image;
    protected boolean destroyed = false;

    // Constructor que setea la posicion en x e y, y el color
    public Brick(int x, int y, String color, Arkanoid ark) {
        super(ark);
        this.setPosition(x, y); // lo posiciona
        setColor(color); // color, con el puntaje y cantidad de golpes para romperse
        this.bonus = randBonus(); // genera un bonus aleatorio
        golpesDados = 0;
    }

    // Setea la imagen segun el color y asigna puntaje y cantidad de golpes segun el
    // color
    private void setColor(String color) {
        this.color = color;
        try {
            image = ImageIO.read(new File("./imagenes/Bricks/" + this.color + ".png"));
            image = image.getScaledInstance(44, 20, Image.SCALE_SMOOTH);
        } catch (IOException e) {
        }

        switch (this.color) {
        case "White":
            this.puntaje = 50;
            this.golpes = 1;
            break;
        case "Orange":
            this.puntaje = 60;
            this.golpes = 1;
            break;
        case "LBlue":
            this.puntaje = 70;
            this.golpes = 1;
            break;
        case "Green":
            this.puntaje = 80;
            this.golpes = 1;
            break;
        case "Red":
            this.puntaje = 90;
            this.golpes = 1;
            break;
        case "Blue":
            this.puntaje = 100;
            this.golpes = 1;
            break;
        case "Purple":
            this.puntaje = 110;
            this.golpes = 1;
            break;
        case "Yellow":
            this.puntaje = 120;
            this.golpes = 1;
            break;
        case "Silver":
            this.puntaje = 120;
            this.golpes = 2;
            break;
        case "Gold":
            this.puntaje = 0;
            this.golpes = 10000;
            break;
        default:
            break;
        }
    }

    // Devuelve si el bloque tiene o no bonus
    public boolean hasBonus() {
        return this.bonus == null ? false : true;
    }

    // Devuelve bonus de un bloque
    public Bonus getBonus() {
        return this.bonus;
    }

    // Setea destruccion
    public void setDestroyed() {
        destroyed = true;
    }

    // Devuelve si esta destruido
    public boolean getDestroyed() {
        return destroyed;
    }

    // Devuelve puntaje de un bloque
    public int getScore() {
        return this.puntaje;
    }

    public int cantGolpes() {
        return this.golpes;
    }

    public int getGolpesDados() {
        return this.golpesDados;
    }

    public void golpear() {
        this.golpesDados++;
    }

    public String getColor() {
        return this.color;
    }

    // Devuelve un bonus random
    private Bonus randBonus() {
        Random r = new Random();
        Integer i = r.nextInt(200);
        Bonus b = null;

        if (0 < i && i < 5) {
            b = new Duplicate(ark);
        }
        if (10 < i && i < 20) {
            b = new Enlarge(ark);
        }
        if (94 < i && i < 96) {
            b = new Warp(ark);
        }
        if (30 < i && i < 35) {
            b = new ExtraPlayer(ark);
        }
        if (40 < i && i < 55) {
            b = new Slow(ark);
        }
        if (60 < i && i < 75) {
            b = new Catch(ark);
        }
        if (80 < i && i < 90) {
            b = new Laser(ark);
        }
        return (b);
    }

    public void draw(Graphics2D g) {
        g.drawImage(image, (int) super.getX(), (int) super.getY(), null);
    }

    // Para colisiones
    double left() {
        return (getX());
    }

    double right() {
        return (getX() + image.getWidth(null));
    }

    double top() {
        return (getY() - image.getHeight(null) / 2.0);
    }

    double bottom() {
        return (getY() + image.getHeight(null) / 2.0);
    }

    void update(double delta) {
    }
}