import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

public class SettingsPanel extends JPanel implements ActionListener {
	private String buttonLabel[] = { "FullScreen", "Sound", "OriginalMusic", "Paddle", "Controls", "Save", "Reset" };
	private JToggleButton[] button;
	private JLabel title;
	private Image background_img;
	protected Properties gameproperties = new Properties();
	protected String fileProperties;

	public SettingsPanel(String fileProps) {
		fileProperties = fileProps;
		setLayout(new GridBagLayout());
		button = new JToggleButton[7];
		readPropertiesFile();
		initComponents();
		setSize(800, 600);
		background_img = new ImageIcon("imagenes/background_img.jpg").getImage();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(background_img, 0, 0, getWidth(), getHeight(), this);
	}

	private void initComponents() {
		title = new JLabel("Settings");
		title.setForeground(Color.white);
		title.setFont(new Font("Helvetica", Font.BOLD, 20));
		title.setIcon(new ImageIcon("imagenes/settings.png"));
		title.setIconTextGap(8);
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = 2;
		c.gridheight = 2;
		c.weightx = 1;
		c.weighty = 1;
		c.gridx = 1;
		c.gridy = 0;
		add(title, c);
		for (int i = 0; i < buttonLabel.length; i++) {
			if (buttonLabel[i] == "Save")
				c.gridx -= 4;
			if (buttonLabel[i] == "Reset") {
				c.gridx += 3;
				c.gridy -= 2;
			}
			c.gridy += 2;
			button[i] = new JToggleButton(buttonLabel[i]);
			button[i].setIcon(new ImageIcon("imagenes/" + buttonLabel[i] + ".png"));
			button[i].setSelectedIcon(new ImageIcon("imagenes/" + buttonLabel[i] + "Off.png"));
			button[i].setFocusable(false);
			button[i].setBackground(Color.blue);
			button[i].addActionListener(this);
			button[i].setBackground(Color.WHITE);
			button[i].setOpaque(false);
			button[i].setBorderPainted(false);
			button[i].setForeground(Color.white);
			button[i].setFont(new Font("Helvetica", Font.BOLD, 15));
			button[i].setIconTextGap(6);
			if (buttonLabel[i] != "Save" && buttonLabel[i] != "Reset" && buttonLabel[i] != "Controls")
				button[i].setSelected(!Boolean.parseBoolean(gameproperties.getProperty(buttonLabel[i]))); // Lee de la
																											// configuracion
																											// para
																											// determinar
																											// si el
																											// boton
																											// estara
																											// seleccionado
																											// o no
			add(button[i], c);
		}
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getActionCommand() == "Save") {
			if (button[0].isSelected()) {
				gameproperties.setProperty("FullScreen", "false");
			} else
				gameproperties.setProperty("FullScreen", "true");
			if (button[1].isSelected())
				gameproperties.setProperty("Sound", "false");
			else
				gameproperties.setProperty("Sound", "true");
			if (button[2].isSelected()) {
				gameproperties.setProperty("OriginalMusic", "false");
			} else
				gameproperties.setProperty("OriginalMusic", "true");
			if (button[3].isSelected())
				gameproperties.setProperty("Paddle", "false");
			else
				gameproperties.setProperty("Paddle", "true");
			button[5].setSelected(false); // Deselecciono Save

			// Guarda en el archivo todas estas nuevas configuraciones
			try {
				FileOutputStream out = new FileOutputStream(fileProperties);
				gameproperties.store(out, null);
				out.close();
			} catch (Exception e) {

			}
		}

		// Si se aprieta "Reset", vuelvo a la configuracion por defecto y no se aplican
		// los acmbios
		if (evt.getActionCommand() == "Reset") {
			for (int i = 0; i < buttonLabel.length; i++)
				button[i].setSelected(false);
			button[5].setSelected(false);
		}

		if (evt.getActionCommand() == "Controls") {
			JDialog jd = new JDialog();
			JTextField keyRight, keyLeft, keyShooting;
			keyRight = new JTextField(KeyEvent.getKeyText(Integer.parseInt(gameproperties.getProperty("RIGHT"))), 25);
			keyLeft = new JTextField(KeyEvent.getKeyText(Integer.parseInt(gameproperties.getProperty("LEFT"))), 25);
			keyShooting = new JTextField(KeyEvent.getKeyText(Integer.parseInt(gameproperties.getProperty("SHOOTING"))), 25);
			keyRight.setEditable(false);
			keyLeft.setEditable(false);
			keyShooting.setEditable(false);
			jd.setModal(true);
			
			jd.setLayout(new GridLayout(0, 1));
			jd.setLocationRelativeTo(null);
			jd.setBackground(Color.BLUE);
			JButton LEFT = new JButton("LEFT");
			jd.add(LEFT);
			jd.add(keyLeft);
			JButton RIGHT = new JButton("RIGHT");
			jd.add(RIGHT);
			jd.add(keyRight);
			JButton SHOOTING = new JButton("SHOOTING");
			jd.add(SHOOTING);
			jd.add(keyShooting);
			KeyListener listener = new KeyListener() {
				public void keyPressed(KeyEvent arg0) {
					switch(((JButton)jd.getFocusOwner()).getActionCommand()){
					case "LEFT":{
						keyLeft.setText(arg0.getKeyText(arg0.getKeyCode()));
						gameproperties.setProperty("LEFT", Integer.toString(arg0.getKeyCode()));
						break;
					}
					case "RIGHT":{
						keyRight.setText(arg0.getKeyText(arg0.getKeyCode()));
						gameproperties.setProperty("RIGHT", Integer.toString(arg0.getKeyCode()));
						break;
					}
					case "SHOOTING":{
						keyShooting.setText(arg0.getKeyText(arg0.getKeyCode()));
						gameproperties.setProperty("SHOOTING", Integer.toString(arg0.getKeyCode()));

					}
					}
				}

				public void keyReleased(KeyEvent arg0) {
				}

				public void keyTyped(KeyEvent arg0) {
				}
			};
			
			jd.addKeyListener(listener);
			RIGHT.addKeyListener(listener);
			LEFT.addKeyListener(listener);
			SHOOTING.addKeyListener(listener);
			jd.setSize(400, 200);
			jd.setVisible(true);

		}

	}

	protected void readPropertiesFile() {

		try {
			FileInputStream in = new FileInputStream(fileProperties);
			gameproperties.load(in);
			in.close();
		} catch (IOException e) {
			System.out.println("Error en metodo  readPropertiesFile(): " + e);
		}

	}
}
