import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

class GameSystem extends JFrame implements ActionListener, ListSelectionListener, MenuListener {
    private JList<String> lista; // lista de juegos
    private JButton startGame; // botones
    private JPanel contenedor; // JPanel que contiene todos los componentes
    private JPanel panelLateral; // panel de botones
    private JPanel panelCentral;// panel para la imagen
    private JLabel gameImg; // donde van las imagenes
    private JMenuBar bar;
    private JMenu about,exit;

    public GameSystem() { // Constructor
        super("Game System"); // nombre frame
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        FXPlayer.GAMESYSTEM.play();
        FXPlayer.GAMESYSTEM.stop();
        try {
            // Instanciando objetos
            String juegos[] = { "Arkanoid", "Sunset Riders", "Mario Bros" }; // array de juegos

            // Botones
            startGame = new JButton("START GAME"); // boton

            // Jlist
            lista = new JList<String>(juegos); // lista de juegos

            // JPanels y JLabel
            contenedor = new JPanel(); // contenedor de paneles
            panelLateral = new JPanel();
            panelCentral = new JPanel();
            gameImg = new JLabel();

            // Barra de menu
            bar = new JMenuBar();
            about = new JMenu("About");
            exit = new JMenu("Exit");
            bar.add(about);
            bar.add(exit);

            // listeners para menu
            about.addMenuListener(this);
            exit.addMenuListener(this);

            // setlayouts
            contenedor.setLayout(new BorderLayout());
            panelLateral.setLayout(new BorderLayout());
            panelCentral.setLayout(new BorderLayout());

            // añado al panel lateral
            panelLateral.add(new JScrollPane(lista)); // lista de juegos

            // añado al panel central
            panelCentral.add(startGame, BorderLayout.SOUTH); // boton jugar

            // añado al contenedor principal
            contenedor.add(panelLateral, BorderLayout.WEST);
            contenedor.add(panelCentral, BorderLayout.CENTER);

            // añado los componentes al frame
            this.panelCentral.add(gameImg);
            this.add(contenedor);

            // añado los listeners
            startGame.addActionListener(this);
            lista.addListSelectionListener(this);

            // colores
            panelCentral.setBackground(Color.BLACK);
            lista.setBackground(Color.BLACK);
            lista.setForeground(Color.WHITE);
            startGame.setBackground(Color.BLACK);
            startGame.setForeground(Color.WHITE);
            bar.setBackground(Color.BLACK);
            about.setForeground(Color.WHITE);
            exit.setForeground(Color.WHITE);

            // configuraciones del frame
            this.setUndecorated(true);
            Color color = UIManager.getColor("activeCaptionBorder");
            this.getRootPane().setBorder(BorderFactory.createLineBorder(color, 4));
            this.setJMenuBar(bar);
            this.setSize(800, 600); // tamaño de la ventana
            this.setLocationRelativeTo(null); // centrarla en la pantalla
            this.setVisible(true);
            this.setIconImage(Toolkit.getDefaultToolkit().getImage("imagenes/iconoGameSystem.png"));
            this.setResizable(false); // no se puede cambiar el tamaño de la ventana

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand() == startGame.getActionCommand()) {
            if (lista.getSelectedValue() == "Arkanoid") {
                Menu menu = new Menu();
            }
        }
    }

    public void valueChanged(ListSelectionEvent l) {
        ImageIcon img = new ImageIcon("imagenes/" + lista.getSelectedValue() + ".png");
        img = new ImageIcon(img.getImage().getScaledInstance(this.panelCentral.getWidth(),
                this.panelCentral.getHeight(), Image.SCALE_SMOOTH));
        gameImg.setIcon(img);
        this.pack();
        this.setSize(800, 600);
    }

    @Override
    public void menuSelected(MenuEvent e) {
        if (e.getSource().equals(about)) {
            JOptionPane.showMessageDialog(null,
                    "Este es el proyecto final para la materia Programacion Orientada a Objetos.\nDiseñado e implementado por Sofia Rodriguez y Pablo Torres.",
                    "About", JOptionPane.INFORMATION_MESSAGE);
        }
        if (e.getSource().equals(exit)){
            this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
    }

    @Override
    public void menuDeselected(MenuEvent e) {

    }

    @Override
    public void menuCanceled(MenuEvent e) {

    }

    public static void main(String[] args) {
        GameSystem gs = new GameSystem();
    }

}