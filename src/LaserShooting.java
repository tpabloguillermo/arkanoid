
public class LaserShooting extends GraphicObject {
	private final String laserShooting = "imagenes/laserShooting.png";

	public LaserShooting(Arkanoid ark) {
		super(ark);
		setImage(laserShooting);
		velocityY = 400;
	}

	double left() {
		return getX() - getWidth() / 2;
	}

	double right() {
		return getX() + getWidth() / 2;
	}

	double top() {
		return getY() - getWidth() / 2;
	}

	double bottom() {
		return getY() + getWidth() / 2;
	}

	public void resetPosition() {
		setPosition(ark.paddle.getX() + ark.paddle.getSizeX() / 2.0 - getWidth() / 2, ark.paddle.getY());

	}

	void update(double delta) {
		if (getY() < ark.background.top())
			resetPosition();
		setY(getY() - velocityY * delta);
	}

}
