import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Point2D;

import javax.swing.ImageIcon;

public abstract class GraphicObject {
	private Point2D.Double position = new Point2D.Double();
	protected double velocityX, velocityY;
	protected Image img;
	protected Arkanoid ark;

	public GraphicObject(Arkanoid arkanoid) {
		ark = arkanoid;
	}

	public void setImage(String imgStr) {
		img = new ImageIcon(imgStr).getImage();
	}

	public void draw(Graphics2D g) {
		g.drawImage(img, (int) getX(), (int) getY(), null);
	}

	public Image getImage() {
		return img;
	}

	public double getHeight() {
		return img.getHeight(null);
	}

	public double getWidth() {
		return img.getWidth(null);
	}

	public double getX() {
		return position.getX();
	}

	public double getY() {
		return position.getY();
	}

	public void setX(double x) {
		this.position.x = x;
	}

	public void setY(double y) {
		this.position.y = y;
	}

	protected double getVelocityX() {
		return velocityX;
	}

	protected void setVelocityX(double velX) {
		velocityX = velX;
	}

	protected double getVelocityY() {
		return velocityX;
	}

	protected void setVelocityY(double velY) {
		velocityX = velY;
	}

	public void setPosition(double x, double y) {
		position.setLocation(x, y);
	}

	// Los siguientes metodos abstractos sirven para detectar las posiciones y
	// chequear colisiones
	abstract double left();

	abstract double right();

	abstract double top();

	abstract double bottom();

	abstract void update(double delta);
}
