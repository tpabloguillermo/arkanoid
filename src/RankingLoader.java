import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

class RankingLoader {
    ItemRecord records[] = new ItemRecord[10];
    JsonArray jsonArray = new JsonArray();
    JsonParser jParser = new JsonParser();
    JsonObject jsonObject = new JsonObject();

    // añade a partir de un array primitivo de records, todos los records a un
    // archivo json
    public RankingLoader(String nombre, Arkanoid ark) {
        Gson gson = new Gson();
        getRanking();
        insertInRanking(nombre, ark);
        for (int i = 0; i < records.length; i++) {
            jsonArray.add(jParser.parse(gson.toJson(this.records[i])));
        }
        try {
            jsonObject.add("puntajes", jsonArray);
            FileWriter fw = new FileWriter("ranking.json");
            fw.write(jsonObject.toString());
            fw.close();

        } catch (IOException e) {
        }
    }

    // lee el ranking del archivo json
    private void getRanking() {
        Gson gson = new Gson();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("ranking.json"));
            Player resultado = gson.fromJson(br, Player.class);
            if (resultado != null) {
                int i = 0;
                for (ItemRecord res : resultado.getRecord()) {
                    this.records[i] = res;
                    i++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // este metodo inserta al nuevo record en el ranking
    // solamente si es mayor a algun record que encuentre, si es igual lo
    // sobreescribe
    // si es menor entonces a todos no lo inserta
    private void insertInRanking(String nombre, Arkanoid ark) {
        ItemRecord nuevoRecord = new ItemRecord();
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        nuevoRecord.addRecord(nombre, ark.getScore(), date.format(formatter).toString(), ark.getLevel());
        for (int i = 0; i < records.length; i++) {
            if (nuevoRecord.getScore() >= records[i].getScore()) {
                ItemRecord aux, aux2 = new ItemRecord();
                aux = records[i];
                records[i] = nuevoRecord;
                for (int j = i + 1; j < records.length; j++) {
                    aux2 = records[j];
                    records[j] = aux;
                    aux = aux2;
                }
                break;
            }
        }
    }
}