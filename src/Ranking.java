import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.google.gson.Gson;

import java.io.FileReader;

import java.awt.FontFormatException;

/* Esta clase lee de un archivo JSON el ranking y lo 
muestra por pantalla en un formato adecuado */

class Ranking extends JPanel {
    protected ItemRecord records[] = new ItemRecord[10];
    protected JLabel title;
    private Image background_img;
    protected String fontStr = "fuentes/arcade.ttf";
    protected static Font font;

    public Ranking() {
        this.setLayout(new GridBagLayout());
        registerFont(fontStr);
        getRanking();
        order();
        background_img = new ImageIcon("imagenes/background_img.jpg").getImage();
    }

    public void paintComponent(Graphics g) {
        int paso = 80;
        if(records.length == 0){
            g.drawImage(background_img, 0, 0, getWidth(), getHeight(), null);
            g.setFont(font);
            g.setColor(Color.white);
            g.drawString("You haven't played yet \n Play to see the ranking", 300, 100);
        }else{
            g.drawImage(background_img, 0, 0, getWidth(), getHeight(), null);
            g.setFont(font);
            g.setColor(Color.white);
            g.drawString("Score", 20, 80);
            g.drawString("Name", 220, 80);
            g.drawString("Date", 420, 80);
            g.drawString("Level", 620, 80);
            g.drawString("Ranking", 300, 40);
            g.setFont(new Font(font.getName(), Font.BOLD, 10));
            for (int i = 0; i < records.length; i++) {
                paso += 40;
                g.drawString(records[i].getScore().toString(), 20, paso);
                g.drawString(records[i].getName(), 220, paso);
                g.drawString(records[i].getDate(), 420, paso);
                g.drawString(records[i].getLevel().toString(), 620, paso);
            }
        }
    }

    // ordena el ranking cada vez que se almacena en el vector cuando se lee el
    // archivo json
    private void order() {
        ItemRecord temp = null;
        for (int i = 0; i < this.records.length; i++) {
            for (int j = 0; j < this.records.length-1; j++) {
                if (this.records[j].getScore() < this.records[j + 1].getScore()) {
                    temp = this.records[j];
                    this.records[j] = this.records[j + 1];
                    this.records[j + 1] = temp;
                }
            }
        }
    }

    public static boolean registerFont(String fontFile) {
        boolean b = false;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, new File(fontFile)).deriveFont(24f);
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
            b = true;
        } catch (FontFormatException | IOException e) {
            b = false;
        }
        return b;
    }

    // lee el ranking del archivo json
    private void getRanking() {
        Gson gson = new Gson();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("ranking.json"));
            Player resultado = gson.fromJson(br, Player.class);
            if (resultado != null) {
                int i = 0;
                for (ItemRecord res : resultado.getRecord()) {
                    this.records[i] = res;
                    i++;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}