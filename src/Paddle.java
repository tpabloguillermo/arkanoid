public class Paddle extends GraphicObject {
	private String originalPaddle = "imagenes/Paddle.png";
	private String specialPaddle = "imagenes/PaddleOff.png";
	private String enlarge = "imagenes/paddleEnlarged.png";
	private String laserPaddle = "imagenes/paddleLaser.png";
	private boolean enlarged = false;
	private boolean laser = false;
	private String currentImage;
	private static double paddle_velocity = 300.0;

	public Paddle(Arkanoid ark) {
		super(ark);
		velocityX = paddle_velocity;
		if (Boolean.parseBoolean(ark.gameProps.getProperty("Paddle"))) {
			currentImage = originalPaddle; // las guardamos para cuando haya que hacer cambios de nave
			setImage(originalPaddle);
		} else {
			currentImage = specialPaddle;
			setImage(specialPaddle);
			enlarge = "imagenes/paddleEnlargedOff.png";
		}
	}

	double left() {
		return (getX());
	}

	double right() {
		return (getX() + getWidth());
	}

	double top() {
		return (getY());
	}

	double bottom() {
		return (getY() + getHeight());
	}

	public int getSizeX() {
		return (int)getWidth();
	}

	public int getSizeY() {
		return (int) getHeight();
	}

	public void setEnlarged() {
		enlarged = true;
		setImage(enlarge);
	}

	public boolean isEnlarged() {
		return enlarged;
	}

	public void disableEnlarged() {
		enlarged = false;
		setImage(currentImage);
	}

	public void setLaser() {
		laser = true;
		setImage(laserPaddle);
	}
	
	public boolean laser() {
		return laser;
	}
	
	public void disableLaser() {
		laser = false;
		setImage(currentImage);
	}
	
	public void warp() {
		setX(getX() + (Math.abs(getVelocityX()) * 0.02));
	}

	void update(double delta) {
		// Movimiento de la nave
		// Al presionar tecla izquierda
		if (ark.getKeyboard().isKeyPressed(Integer.parseInt(ark.gameProps.getProperty("LEFT")))) {
			if (left() > ark.background.left())
				setVelocityX(-paddle_velocity);
			else
				setVelocityX(0.0);
			setX(getX() + (getVelocityX() * delta));
		}
		// Al presionar tecla derecha
		if (ark.getKeyboard().isKeyPressed(Integer.parseInt(ark.gameProps.getProperty("RIGHT")))) {
			if (right() < ark.background.right()) {
				setVelocityX(paddle_velocity);
			} else
				setVelocityX(0.0);
			setX(getX() + (getVelocityX() * delta));
		}
	}

}
